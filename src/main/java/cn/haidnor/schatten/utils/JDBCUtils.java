package cn.haidnor.schatten.utils;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * 封装了 JDBC 常用的操作
 *
 * @author Haidnor
 */
public class JDBCUtils {

    /**
     * 给 preparedStatement 设置参数
     *
     * @param preparedStatement 预编译 SQL 语句的对象
     * @param params            参数
     */
    public static void handleParams(PreparedStatement preparedStatement, Object[] params) {
        if (params != null) {
            for (int i = 0; i < params.length; i++) {
                try {
                    preparedStatement.setObject(i + 1, params[i]);
                } catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                }
            }
        }
    }

}
