package cn.haidnor.schatten.utils;

import cn.haidnor.schatten.bean.ColumnInfo;
import cn.haidnor.schatten.bean.JavaFieldGetSet;
import cn.haidnor.schatten.bean.TableInfo;
import cn.haidnor.schatten.core.DBManager;
import cn.haidnor.schatten.core.TypeConvertor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 封装生成 Java 文件(源代码)常用的操作
 *
 * @author Haidnor
 */
public class JavaFileUtils {

    /**
     * 根据字段信息生成java属性信息,如 varchar username -> private String username
     * 以及相应的 set 和 get 方法
     *
     * @param columnInfo 字段信息
     * @param convertor  类型转换器
     * @return Java属性和set/get方法源码
     */
    public static JavaFieldGetSet creatFiledGetSetSRC(ColumnInfo columnInfo, TypeConvertor convertor) {
        JavaFieldGetSet jfgs = new JavaFieldGetSet();

        String javaFileType = convertor.databaseType2JavaType(columnInfo.getDataType());

        jfgs.setFieldInfo("    " + "private " + javaFileType + " " + columnInfo.getName() + ";\n");

        // 生成get方法源代码
        // public String getUserName(){return username;}
        StringBuilder getSrc = new StringBuilder();
        getSrc.append("    " + "public " + javaFileType + " get"
                + StringUtils.firstChar2UpperCase(columnInfo.getName())
                + "() {\n");
        getSrc.append("        " + "return " + columnInfo.getName() + ";\n");
        getSrc.append("    " + "}\n");
        jfgs.setGetInfo(getSrc.toString());

        // 生成set方法的源代码
        // public void setUserName(String username){this.username = username}
        StringBuilder setSrc = new StringBuilder();
        setSrc.append("    " + "public void" + " set"
                + StringUtils.firstChar2UpperCase(columnInfo.getName())
                + "(");
        setSrc.append(javaFileType + " " + columnInfo.getName());
        setSrc.append(") {\n");
        setSrc.append("        " + "this." + columnInfo.getName() + " = " + columnInfo.getName() + ";\n");
        setSrc.append("    " + "}\n");
        jfgs.setSetInfo(setSrc.toString());

        return jfgs;
    }

    /**
     * 根据表信息生成 Java 类的源代码
     *
     * @param tableInfo 数据库表信息
     * @param convertor 数据类型转化器
     * @return java 类的源代码
     */
    public static String creatJavaSrc(TableInfo tableInfo, TypeConvertor convertor) {
        StringBuilder src = new StringBuilder();

        Map<String, ColumnInfo> columns = tableInfo.getColumns();
        List<JavaFieldGetSet> javaFields = new ArrayList<JavaFieldGetSet>();

        for (ColumnInfo c : columns.values()) {
            javaFields.add(creatFiledGetSetSRC(c, convertor));
        }

        // 生成 package 语句
        src.append("package " + DBManager.getConf().getPoPackage() + ";\n\n");
        // 生成import
        src.append("import java.sql.*;\n");
        src.append("import java.util.*;\n\n");

        // 生成类声明语句
        src.append("public class " + StringUtils.firstChar2UpperCase(tableInfo.getTableName()) + " {\n\n");

        // 生成属性列表
        for (JavaFieldGetSet f : javaFields) {
            src.append(f.getFieldInfo() + "\n");
        }
        //生成 set 方法
        for (JavaFieldGetSet f : javaFields) {
            src.append(f.getSetInfo());
            src.append('\n');
        }
        //生成 get 方法
        for (JavaFieldGetSet f : javaFields) {
            src.append(f.getGetInfo());
            src.append('\n');
        }
        // 结束
        src.append("}\n");
        return src.toString();
    }

    /**
     * 创建与数据库表线对应的 Java 源文件
     *
     * @param tableInfo 表信息
     * @param convertor 类型转换器,需要对应不同的数据库,目前只支持MySQL
     */
    public static void createJavaPOFile(TableInfo tableInfo, TypeConvertor convertor) {
        String src = creatJavaSrc(tableInfo, convertor);

        String srcPath = DBManager.getConf().getSrcPath() + "\\";
        String packagePath = DBManager.getConf().getPoPackage().replace(".", "\\");

        File f = new File(srcPath + packagePath);

        // 如果指定目录不存在,则自动建立目录
        if (!f.exists()) {
            f.mkdirs();
        }
        File code = new File(srcPath + packagePath + "\\"
                + StringUtils.firstChar2UpperCase(tableInfo.getTableName()) + ".java");
        try {
            code.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(code))) {
            writer.write(src);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
