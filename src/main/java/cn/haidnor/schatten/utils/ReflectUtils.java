package cn.haidnor.schatten.utils;

import java.lang.reflect.Method;

/**
 * 封装了反射常用的操作
 *
 * @author Haidnor
 */
public class ReflectUtils {

    /**
     * 调用对象对应属性的 filedName 的 get 方法
     *
     * @param fieldName
     * @param obj
     * @return
     */
    public static Object invokeGet(String fieldName, Object obj) {
        try {
            Class<?> c = obj.getClass();
            Method method = c.getDeclaredMethod("get" + StringUtils.firstChar2UpperCase(fieldName));
            return method.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 调用对象的 filedName 的 set 方法
     *
     * @param fieldName
     * @param bean
     * @param columnValue
     */
    public static void invokeSet(String fieldName, Object bean, Object columnValue) {
        if (columnValue != null) {
            try {
                Class<?> aClass = bean.getClass();
                Method method = aClass.getDeclaredMethod("set" + StringUtils.firstChar2UpperCase(fieldName)
                        , columnValue.getClass());
                method.invoke(bean, columnValue);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
