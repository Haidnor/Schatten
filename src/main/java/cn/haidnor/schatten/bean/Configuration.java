package cn.haidnor.schatten.bean;

/**
 * 管理数据库连接的配置信息
 *
 * @author Haidnor
 */
public class Configuration {
    /**
     * 数据库驱动类
     */
    private String driver;

    /**
     * JDBC数据库连接
     */
    private String url;

    /**
     * 数据库连接用户名
     */
    private String user;

    /**
     * 数据库连接密码
     */
    private String password;

    /**
     * 使用的数据库
     */
    private String usingDB;

    /**
     * 项目的源码路径
     */
    private String srcPath;

    /**
     * 扫描 java 类的包 Persistence object 持久化对象
     */
    private String poPackage;

    /**
     * 所需要使用的数据库查询对象类路径
     */
    private String queryClass;

    /**
     * 数据库连接池最大容量
     */
    private int pool_min_size;

    /**
     * 数据库连接池最小容量
     */
    private int pool_max_size;

    public int getPool_min_size() {
        return pool_min_size;
    }

    public void setPool_min_size(int pool_min_size) {
        this.pool_min_size = pool_min_size;
    }

    public int getPool_max_size() {
        return pool_max_size;
    }

    public void setPool_max_size(int pool_max_size) {
        this.pool_max_size = pool_max_size;
    }

    public String getQueryClass() {
        return queryClass;
    }

    public void setQueryClass(String queryClass) {
        this.queryClass = queryClass;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsingDB() {
        return usingDB;
    }

    public void setUsingDB(String usingDB) {
        this.usingDB = usingDB;
    }

    public String getSrcPath() {
        return srcPath;
    }

    public void setSrcPath(String srcPath) {
        this.srcPath = srcPath;
    }

    public String getPoPackage() {
        return poPackage;
    }

    public void setPoPackage(String poPackage) {
        this.poPackage = poPackage;
    }
}
