package cn.haidnor.schatten.bean;

/**
 * 封装 Java 属性和 get set 方法的源代码
 *
 * @author Haidnor
 */
public class JavaFieldGetSet {
    /**
     * 属性的源码信息 private int userId
     */
    private String fieldInfo;

    /**
     * get方法的源码信息 如 public int getUserId(){}
     */
    private String getInfo;

    /**
     * set 方法的源码信息 如 public void setUserId(){}
     */
    private String setInfo;

    public JavaFieldGetSet(String fieldInfo, String getInfo, String setInfo) {
        this.fieldInfo = fieldInfo;
        this.getInfo = getInfo;
        this.setInfo = setInfo;
    }

    public JavaFieldGetSet() {
    }

    public String getFieldInfo() {
        return fieldInfo;
    }

    public void setFieldInfo(String fieldInfo) {
        this.fieldInfo = fieldInfo;
    }

    public String getGetInfo() {
        return getInfo;
    }

    public void setGetInfo(String getInfo) {
        this.getInfo = getInfo;
    }

    public String getSetInfo() {
        return setInfo;
    }

    public void setSetInfo(String setInfo) {
        this.setInfo = setInfo;
    }

    @Override
    public String toString() {
        System.out.println(fieldInfo);
        System.out.println(getInfo);
        System.out.println(setInfo);
        return super.toString();
    }
}
