package cn.haidnor.schatten.core;

import cn.haidnor.schatten.bean.Configuration;
import cn.haidnor.schatten.core.pool.DBConnectionPool;

import java.io.IOException;
import java.sql.*;
import java.util.Properties;

/**
 * 根据配置信息,维持连接对象的管理(增加连接池功能)
 *
 * @author Haidnor
 */
public class DBManager {

    /**
     * 封装所有的配置信息
     */
    private static final Configuration CONFIG_INFO;
    /**
     * 数据库连接池
     */
    private static DBConnectionPool pool;

    // 初始化 db.properties 配置信息
    static {
        CONFIG_INFO = new Configuration();

        Properties pros = new Properties();

        try {
            pros.load(Thread.currentThread()
                    .getContextClassLoader()
                    .getResourceAsStream("db.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        CONFIG_INFO.setDriver(pros.getProperty("driver"));
        CONFIG_INFO.setUrl(pros.getProperty("url"));
        CONFIG_INFO.setUser(pros.getProperty("user"));
        CONFIG_INFO.setPassword(pros.getProperty("password"));
        CONFIG_INFO.setUsingDB(pros.getProperty("usingDB"));
        CONFIG_INFO.setSrcPath(pros.getProperty("srcPath"));
        CONFIG_INFO.setPoPackage(pros.getProperty("poPackage"));
        CONFIG_INFO.setQueryClass(pros.getProperty("queryClass"));
        CONFIG_INFO.setPool_max_size(Integer.parseInt(pros.getProperty("pool_max_size")));
        CONFIG_INFO.setPool_min_size(Integer.parseInt(pros.getProperty("pool_min_size")));
    }

    /**
     * 创建数据库连接对象
     *
     * @return 数据库连接对象
     */
    public static Connection createConnection() {
        try {
            return DriverManager.getConnection(CONFIG_INFO.getUrl(),
                    CONFIG_INFO.getUser(),
                    CONFIG_INFO.getPassword());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取数据库连接对象.
     * 这里使用了懒加载,在第一次获取连接的时候加载数据库连接池
     *
     * @return 数据库连接对象
     */
    public static Connection getConnection() {
        if (pool == null) {
            pool = new DBConnectionPool();
        }
        return pool.getConnection();
    }

    /**
     * 关闭数据库连接
     *
     * @param rs   查询结果集对象
     * @param ps   SQL操作对象
     * @param conn 数据库连接对象
     */
    public static void close(ResultSet rs, Statement ps, Connection conn) {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        try {
            if (ps != null) {
                ps.close();
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    /**
     * 关闭数据库连接
     *
     * @param ps   SQL操作对象
     * @param conn 数据库连接对象
     */
    public static void close(Statement ps, Connection conn) {
        try {
            if (ps != null) {
                ps.close();
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    /**
     * 关闭连接. 如果连接池中的 connection 对象的数量达到了最大值,那么则会将
     * connection 对象真正的关闭, 否则会将 connection 再次放入连接池中
     *
     * @param connection 从连接池中取出的数据库连接对象
     */
    public static void close(Connection connection) {
        if (connection != null) {
            pool.close(connection);
        }
    }

    /**
     * 返回 Configuration 配置信息对象
     *
     * @return Configuration
     */
    public static Configuration getConf() {
        return CONFIG_INFO;
    }
}
