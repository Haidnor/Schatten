package cn.haidnor.schatten.core.pool;

import cn.haidnor.schatten.bean.Configuration;
import cn.haidnor.schatten.core.DBManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * 数据库连接池
 *
 * @author Haidnor
 */
public class DBConnectionPool {

    /**
     * 封装所有的配置信息
     */
    private static final Configuration CONFIG_INFO;

    /**
     * 池内最大连接数
     */
    private static final int POOL_MAX_SIZE;
    /**
     * 池内最小连接数
     */
    private static final int POOL_MIN_SIZE;

    static {
        CONFIG_INFO = DBManager.getConf();
        POOL_MAX_SIZE = CONFIG_INFO.getPool_max_size();
        POOL_MIN_SIZE = CONFIG_INFO.getPool_min_size();
    }

    /**
     * 数据库连接池, 存放数据库连接对象
     */
    private List<Connection> pool;

    /**
     * 数据库连接池唯一构造方法, 初始化连接池时获取池内对象
     */
    public DBConnectionPool() {
        initPool();
    }

    /**
     * 初始化数据库连接池,使池内 connection 对象数量达到最小值
     * 该方法是线程安全的
     */
    public synchronized void initPool() {
        if (pool == null) {
            pool = new ArrayList<>();
        }
        while (pool.size() <= POOL_MIN_SIZE) {
            pool.add(DBManager.createConnection());
        }
    }

    /**
     * 从池内去取出一个 connection 对象.
     * 该方法是线程安全的
     *
     * @return 数据库连接对象
     */
    public synchronized Connection getConnection() {
        int last_connection_index = pool.size() - 1;
        Connection connection = pool.get(last_connection_index);
        pool.remove(last_connection_index);
        return connection;
    }

    /**
     * 将 connection 对象返回到连接池中.
     * 该方法是线程安全的
     *
     * @param connection 不再使用的 connection 对象
     */
    public synchronized void close(Connection connection) {
        if (pool.size() >= POOL_MAX_SIZE) {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException sqlException) {
                    sqlException.printStackTrace();
                }
            }
        } else {
            pool.add(connection);
        }
    }
}
