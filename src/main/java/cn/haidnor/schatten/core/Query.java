package cn.haidnor.schatten.core;

import cn.haidnor.schatten.bean.ColumnInfo;
import cn.haidnor.schatten.bean.TableInfo;
import cn.haidnor.schatten.utils.JDBCUtils;
import cn.haidnor.schatten.utils.ReflectUtils;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

/**
 * 负责查询(对外提供服务的核心类)
 *
 * @author Haidnor
 */
public abstract class Query implements Cloneable {

    /**
     * 模板方法模式将 JDBC 操作封装成模板,便于重用
     *
     * @param sql             sql 语句
     * @param params          sql 参数
     * @param clazz           记录要封装到的 Java Bean 类
     * @param executeCallBack callBack 的匿名实现类,实现回调
     * @return 查询结果
     */
    protected Object executeQueryTemplate(String sql, Object[] params, Class clazz, ExecuteCallBack executeCallBack) {
        Connection connection = DBManager.getConnection();
        PreparedStatement preparedStatement = null;
        Object result = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            JDBCUtils.handleParams(preparedStatement, params);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = executeCallBack.doExecute(connection, preparedStatement, resultSet);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
        } finally {
            DBManager.close(preparedStatement, connection);
        }
        return result;
    }

    /**
     * 直接执行一个DML语句
     *
     * @param sql    sql语句
     * @param params 参数
     * @return 执行sql语句后影响记录的行数
     */
    public int executeDML(String sql, Object... params) {
        Connection connection = DBManager.getConnection();
        PreparedStatement preparedStatement = null;
        int count = 0;
        try {
            preparedStatement = connection.prepareStatement(sql);
            JDBCUtils.handleParams(preparedStatement, params);
            count = preparedStatement.executeUpdate();
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
        } finally {
            DBManager.close(preparedStatement, connection);
        }
        return count;
    }

    /**
     * 将一个对象储存到数据库中
     * 把对象中不为 null 的属性存储到数据库中, 如果数字为 null 则为 0
     *
     * @param obj 要储存的对象
     */
    public int insert(Object obj) {
        Class<?> aClass = obj.getClass();
        TableInfo tableInfo = TableContext.poClassTableMap.get(aClass);

        String tableName = tableInfo.getTableName();
        StringBuilder sql = new StringBuilder("INSERT INTO " + tableName + " (");

        // 存放 sql 参数对象
        List<Object> params = new ArrayList<>();
        // 计算不为空的属性值
        int countNotNullField = 0;

        Field[] fields = aClass.getDeclaredFields();
        for (Field field : fields) {
            String fieldName = field.getName();
            Object fieldValue = ReflectUtils.invokeGet(fieldName, obj);
            if (fieldValue != null) {
                sql.append(fieldName + ",");
                params.add(fieldValue);
                countNotNullField++;
            }
        }
        sql.setCharAt(sql.length() - 1, ')');
        sql.append(" VALUES (");
        for (int i = 0; i < countNotNullField; i++) {
            sql.append("?,");
        }
        sql.setCharAt(sql.length() - 1, ')');

        return executeDML(sql.toString(), params.toArray());
    }

    /**
     * 删除 clazz 表示对应类的表中的记录(指定主键值id的记录)
     *
     * @param clazz  与数据库表对应的类的Class对象
     * @param priKey 主键的值
     */
    public int delete(Class<?> clazz, Object priKey) {
        // 类名与表名对应
        TableInfo tableInfo = TableContext.poClassTableMap.get(clazz);
        // 获取主键
        ColumnInfo onlyPriKey = tableInfo.getOnlyPriKey();
        String sql = "DELETE FROM " + tableInfo.getTableName() + " WHERE " + onlyPriKey.getName() + " = ?";
        return executeDML(sql, priKey);
    }

    /**
     * 删除对象在数据库中对应的记录(对象所在的类,对象的主键的值对应到记录)
     *
     * @param obj 与数据库表对应的类的Class对象
     */
    public int delete(Object obj) {
        Class<?> aClass = obj.getClass();
        TableInfo tableInfo = TableContext.poClassTableMap.get(aClass);
        ColumnInfo onlyPriKey = tableInfo.getOnlyPriKey();

        // 使用反射,调用属性对应的 get 方法,获取到对象主键的值
        Object priKeyValue = ReflectUtils.invokeGet(onlyPriKey.getName(), obj);
        String sql = "DELETE FROM " + tableInfo.getTableName() + " WHERE " + onlyPriKey.getName() + " = ?";
        return executeDML(sql, priKeyValue);
    }

    /**
     * 更新对象对应的记录，并且只更新指定的字段的值
     *
     * @param obj        需要更新的对象
     * @param fieldNames 更新的属性列表
     * @return 执行sql语句后影响记录的行数
     */
    public int update(Object obj, String... fieldNames) {
        Class<?> aClass = obj.getClass();
        List<Object> params = new ArrayList<>();
        TableInfo tableInfo = TableContext.poClassTableMap.get(aClass);
        ColumnInfo priKey = tableInfo.getOnlyPriKey();

        StringBuilder sql = new StringBuilder("UPDATE " + tableInfo.getTableName() + " SET ");

        for (String fieldName : fieldNames) {
            Object fieldValue = ReflectUtils.invokeGet(fieldName, obj);
            params.add(fieldValue);
            sql.append(fieldName + " = ?,");
        }
        sql.setCharAt(sql.length() - 1, ' ');
        sql.append(" WHERE " + priKey.getName() + " = ");

        sql.append(ReflectUtils.invokeGet(priKey.getName(), obj));

        return executeDML(sql.toString(), params.toArray());
    }

    /**
     * 查询返回多行记录,并将记录封装到clazz指定的类的对象中
     *
     * @param sql    查询语句
     * @param clazz  封装数据的javaBean类的class对象
     * @param params sql语句的参数
     * @return 查询到的结果
     */
    public List<?> queryRows(String sql, Class<?> clazz, Object... params) {

        return (List<?>) executeQueryTemplate(sql, params, clazz, (connection, preparedStatement, resultSet) -> {
            List<Object> beanSet = new ArrayList<>();
            try {
                ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
                while (resultSet.next()) {
                    Object bean = clazz.newInstance();
                    for (int i = 1; i < resultSetMetaData.getColumnCount(); i++) {
                        String columnName = resultSetMetaData.getColumnLabel(i);
                        Object columnValue = resultSet.getObject(i);
                        ReflectUtils.invokeSet(columnName, bean, columnValue);
                    }
                    beanSet.add(bean);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return beanSet;
        });
    }

    /**
     * 查询返回一行记录,并将该记录封装到clazz指定的类的对象中
     *
     * @param sql    查询语句
     * @param clazz  封装数据的javaBean类的class对象
     * @param params sql语句的参数
     * @return 查询到的结果
     */
    public Object queryUniqueRow(String sql, Class<?> clazz, Object... params) {
        List<?> queryRows = queryRows(sql, clazz, params);
        return (queryRows != null & queryRows.size() > 0) ? queryRows.get(0) : null;
    }

    /**
     * 查询返回一个值(一行一列),并将该值返回
     *
     * @param sql    查询语句
     * @param params sql语句的参数
     * @return 查询到的结果
     */
    public Object queryValue(String sql, Object... params) {
        return executeQueryTemplate(sql, params, null, (connection, preparedStatement, resultSet) -> {
            Object value = null;
            try {
                while (resultSet.next()) {
                    value = resultSet.getObject(1);
                }
            } catch (Exception sqlException) {
                sqlException.printStackTrace();
            }
            return value;
        });
    }

    /**
     * 查询返回一个数字(一行一列),并将该值返回
     *
     * @param sql    查询语句
     * @param params sql语句的参数
     * @return 查询到的数字
     */
    public Number queryNumber(String sql, Object... params) {
        return (Number) queryValue(sql, params);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
