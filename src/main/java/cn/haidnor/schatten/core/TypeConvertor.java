package cn.haidnor.schatten.core;

/**
 * 复制java数据类型和数据库数据类型的互相转换
 *
 * @author Haidnor
 */
public interface TypeConvertor {
    /**
     * 将数据库类型转换成java的数据类型
     *
     * @param columnType 数据库字段的数据类型
     * @return java的数据类型
     */
    String databaseType2JavaType(String columnType);

    /**
     * 将 Java 数据类型转换成数据库数据类型
     *
     * @param javaDataType Java数据类型
     * @return 数据库数据类型
     */
    String JavaType2DatabaseType(String javaDataType);
}
