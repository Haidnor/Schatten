package cn.haidnor.schatten.core;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * 执行回调接口
 *
 * @author Haidnor
 */
public interface ExecuteCallBack {
    /**
     * 执行回调接口
     *
     * @param connection 数据库连接
     * @param preparedStatement sql 发送对象
     * @param resultSet 结果集
     */
    Object doExecute(Connection connection, PreparedStatement preparedStatement, ResultSet resultSet);
}
