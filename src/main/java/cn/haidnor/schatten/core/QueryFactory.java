package cn.haidnor.schatten.core;

import cn.haidnor.schatten.bean.Configuration;

/**
 * 查询工厂,提供数据库访问操作对象
 *
 * @author Haidnor
 */
public class QueryFactory {

    private static Query prototypeObj;

    static {
        Configuration conf = DBManager.getConf();
        try {
            Class<?> queryClass = Class.forName(conf.getQueryClass());
            prototypeObj = (Query) queryClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private QueryFactory() {
    }

    /**
     * 获取数据库访问对象
     *
     * @return Query 数据库访问对象
     */
    public static Query createQuery() {
        try {
            return (Query) prototypeObj.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

}
