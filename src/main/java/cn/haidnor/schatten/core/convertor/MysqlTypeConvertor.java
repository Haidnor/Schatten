package cn.haidnor.schatten.core.convertor;

import cn.haidnor.schatten.core.TypeConvertor;

/**
 * MySQL 数据库类型和 Java 数据库类型的转换器
 *
 * @author Haidnor
 */
public class MysqlTypeConvertor implements TypeConvertor {

    @Override
    public String databaseType2JavaType(String columnType) {
        String type = columnType.toUpperCase();

        switch (type) {
            case "VARCHAR":
            case "CHAR":
                return "String";
            case "INT":
            case "TINYINT":
            case "SMALLINT":
            case "INTEGER":
                return "Integer";
            case "BIGINT":
                return "Long";
            case "DOUBLE":
            case "FLOAT":
                return "Double";
            case "CLOB":
                return "java.sql.Clob";
            case "BLOB":
                return "java.sql.Blob";
            case "DATE":
                return "java.sql.Date";
            case "TIME":
                return "java.sql.Time";
            case "DATETIME":
            case "TIMESTAMP":
                return "java.sql.Timestamp";
            case "DECIMAL":
                return "java.math.BigDecimal";
            default:
                return null;
        }
    }

    @Override
    public String JavaType2DatabaseType(String javaDataType) {
        return null;
    }
}
