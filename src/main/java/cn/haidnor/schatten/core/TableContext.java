package cn.haidnor.schatten.core;

import cn.haidnor.schatten.bean.ColumnInfo;
import cn.haidnor.schatten.bean.TableInfo;
import cn.haidnor.schatten.core.convertor.MysqlTypeConvertor;
import cn.haidnor.schatten.utils.JavaFileUtils;
import cn.haidnor.schatten.utils.StringUtils;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 负责管理数据库所有表结构和类结构的关系,并可以根据表结构生成类结构
 *
 * @author Haidnor
 */
public class TableContext {

    /**
     * 表名为 key,表信息对象为 value
     */
    public static Map<String, TableInfo> tables = new HashMap<String, TableInfo>();

    /**
     * 将 po 的 class 对象和表信息对象关联起来,以便重用
     */
    public static Map<Class<?>, TableInfo> poClassTableMap = new HashMap<Class<?>, TableInfo>();

    // 读取数据库,获取元数据,将每一个字段和表的数据封装进对象。最后放入 TableContext.tables 集合中
    static {
        try {
            // 初始化获取数据库表的信息
            Connection conn = DBManager.getConnection();
            // 获取数据库元信息
            DatabaseMetaData metaData = conn.getMetaData();
            // 获取数据库中所有的表信息(包含URL连接以外其它的库)
            ResultSet tableResultSet = metaData.getTables(null,
                    "%",
                    "%",
                    new String[]{"TABLE"});

            while (tableResultSet.next()) {
                String tableName = (String) tableResultSet.getObject("TABLE_NAME");
                // 封装表信息
                TableInfo tableInfo = new TableInfo(tableName,
                        new ArrayList<ColumnInfo>(),
                        new HashMap<String, ColumnInfo>());

                tables.put(tableName, tableInfo);

                // 根据表获取所有的字段信息
                ResultSet columnsSet = metaData.getColumns(null,
                        "%",
                        tableName,
                        "%");
                while (columnsSet.next()) {
                    // 封装 字段名 和 字段类型
                    ColumnInfo columnInfo = new ColumnInfo(columnsSet.getString("COLUMN_NAME"),
                            columnsSet.getString("TYPE_NAME"),
                            0);
                    tableInfo.getColumns().put(columnsSet.getString("COLUMN_NAME"), columnInfo);
                }

                // 根据表名获取主键信息
                ResultSet keySet = metaData.getPrimaryKeys(null, "%", tableName);
                while (keySet.next()) {
                    ColumnInfo columnInfo = tableInfo.getColumns().get(keySet.getObject("COLUMN_NAME"));
                    // 设置为主键类型
                    columnInfo.setKeyType(1);
                    tableInfo.getPriKeys().add(columnInfo);
                }
                // 获取唯一主键。如果是联合主键，则为 null
                if (tableInfo.getPriKeys().size() > 0) {
                    tableInfo.setOnlyPriKey(tableInfo.getPriKeys().get(0));
                }
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        // 更新类结构
        updateJavaPOFile();
        // 加载 PO 包下的类,便于重用提高程序效率
        loadPOTables();
    }


    /**
     * 根据表解耦,更新配置的 po 包下面的 java 类
     */
    public static void updateJavaPOFile() {
        Map<String, TableInfo> tables = TableContext.tables;
        for (TableInfo tableInfo : tables.values()) {
            JavaFileUtils.createJavaPOFile(tableInfo, new MysqlTypeConvertor());
        }
    }

    /**
     * 加载 PO 包下的类
     */
    public static void loadPOTables() {
        for (TableInfo tableInfo : tables.values()) {
            try {
                Class<?> c = Class.forName(DBManager.getConf().getPoPackage()
                        + "."
                        + StringUtils.firstChar2UpperCase(tableInfo.getTableName()));
                poClassTableMap.put(c, tableInfo);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

}
