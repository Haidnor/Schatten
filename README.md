# 概述
**Schatten** 是一款轻量级的对象关系映射数据库框架。其命名源于意为“倒影”的德语单词，代表数据库表与对象的映射关系 （ORM）。**Schatten** 对 JDBC 操作做了轻度封装，使用者只需要编写少量的SQL语句与简单的代码即可实现所需的数据库操作。此外 
 **Schatten** 内置了一个数据库连接池，以便提升数据库操作性能。

# 使用示例
## 向数据库中插入一个对象的数据
```java
Student student = new Student();
student.setStudentNo(40001);
student.setStudentName("Tom");
student.setLoginPwd("123");
student.setSex("male");
student.setGradeID(3);
student.setPhone("18739287330");
student.setAddress("ShangHai");
student.setBornDate(new Timestamp(System.currentTimeMillis()));
student.setEmail("7735921@163.com");

Query query = QueryFactory.createQuery();
int insert = query.update(student);
```

## 修改数据库中的数据
```java
Student student = new Student();
student.setStudentNo(40001);
student.setStudentName("Tom");
student.setLoginPwd("haidnors-sdxxad");

Query query = QueryFactory.createQuery();
int insert = query.update(student, "studentName","loginPwd");
```

## 删除数据库中的数据
```java
Student student = new Student();
student.setStudentNo(40001);

Query query = QueryFactory.createQuery();
int delete = query.delete(student);
```

## 查询数据库中的数据,并自动转换成实体对象
查询多个对象
```java
Query query = QueryFactory.createQuery();
List<?> beans = query.queryRows("SELECT * FROM student WHERE sex = ?", Student.class, "male");
```

查询单个对象
```java
Query query = QueryFactory.createQuery();
Student student = (Student) query.queryUniqueRow("SELECT * FROM student WHERE studentNo = ?", Student.class, "40001");
```

查询单个值
```java
Query query = QueryFactory.createQuery();
Object value = query.queryValue("SELECT COUNT(1) FROM student");
```
